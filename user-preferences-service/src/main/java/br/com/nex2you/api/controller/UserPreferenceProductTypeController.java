package br.com.nex2you.api.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import br.com.nex2you.api.entity.UserPreferenceProductType;
import br.com.nex2you.api.response.Response;
import br.com.nex2you.api.service.UserPreferenceProductTypeService;

@RestController
public class UserPreferenceProductTypeController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UserPreferenceProductTypeService userPreferenceProductTypeService;

	@PostMapping
	public ResponseEntity<Response<UserPreferenceProductType>> create(HttpServletRequest request,
			@RequestBody UserPreferenceProductType userPreferenceProductType, BindingResult result) {
		logger.info("[UserPreferenceProductTypeController][create][request: {}][userPreferenceProductType: {}]",
				request.toString(), userPreferenceProductType.toString());
		Response<UserPreferenceProductType> response = new Response<>();
		try {
			validateCreatePartner(userPreferenceProductType, result);
			if (result.hasErrors()) {
				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
				return ResponseEntity.badRequest().body(response);
			}

			Optional<UserPreferenceProductType> optional = userPreferenceProductTypeService
					.findByUsernameAndProductTypeId(userPreferenceProductType.getProductType().getId(),
							userPreferenceProductType.getUsername());
			if (!optional.isPresent()) {
				UserPreferenceProductType partnerPersisted = (UserPreferenceProductType) userPreferenceProductTypeService
						.createOrUpdate(userPreferenceProductType);
				response.setData(partnerPersisted);
			} else {
				response.setData(optional.get());
			}
			logger.info("[UserPreferenceProductTypeController][create][response: {}]", response.toString());
		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			logger.error("[UserPreferenceProductTypeController][create][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(response);
	}

	@PutMapping
	public ResponseEntity<Response<UserPreferenceProductType>> update(HttpServletRequest request,
			@RequestBody UserPreferenceProductType userPreferenceProductType, BindingResult result) {
		Response<UserPreferenceProductType> response = new Response<>();
		logger.info("[UserPreferenceProductTypeController][update][request: {}][userPreferenceProductType: {}]",
				request.toString(), userPreferenceProductType.toString());
		try {
			validateUpdatePartner(userPreferenceProductType, result);
			if (result.hasErrors()) {
				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
				return ResponseEntity.badRequest().body(response);
			}

			UserPreferenceProductType partnerPersisted = (UserPreferenceProductType) userPreferenceProductTypeService
					.createOrUpdate(userPreferenceProductType);
			response.setData(partnerPersisted);
			logger.info("[UserPreferenceProductTypeController][update][response: {}]", response.toString());
		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			logger.error("[UserPreferenceProductTypeController][update][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "{id}/id")
	public ResponseEntity<Response<UserPreferenceProductType>> findById(
			@RequestHeader("Authorization") String authorization, @PathVariable String id) {
		Response<UserPreferenceProductType> response = new Response<>();
		logger.info("[UserPreferenceProductTypeController][findById][id: {}]", id);

		Optional<UserPreferenceProductType> userPreferenceProductType = userPreferenceProductTypeService
				.findById(authorization, id);

		if (userPreferenceProductType.isPresent()) {
			response.setData(userPreferenceProductType.get());
			logger.info("[UserPreferenceProductTypeController][findById][response: {}]", response.toString());
		} else {
			response.getErrors().add("Register not found for id ".concat(id));
			logger.error("[UserPreferenceProductTypeController][findById][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}

		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "{productTypeId}/{username}")
	public ResponseEntity<Response<UserPreferenceProductType>> findByUsernameAndProductTypeId(
			@PathVariable String productTypeId, @PathVariable String username) {
		Response<UserPreferenceProductType> response = new Response<>();
		logger.info(
				"[UserPreferenceProductTypeController][findByUsernameAndProductTypeId][productTypeId: {}][username: {}]",
				productTypeId, username);

		Optional<UserPreferenceProductType> userPreferenceProductType = userPreferenceProductTypeService
				.findByUsernameAndProductTypeId(productTypeId, username);

		if (userPreferenceProductType.isPresent()) {
			response.setData(userPreferenceProductType.get());
			logger.info("[UserPreferenceProductTypeController][findByUsernameAndProductTypeId][response: {}]",
					response.toString());
		} else {
			response.getErrors().add("Register not found");
			logger.error("[UserPreferenceProductTypeController][findByUsernameAndProductTypeId][response: {}]",
					response.toString());
		}

		return ResponseEntity.ok(response);
	}

	@DeleteMapping(value = "{productTypeId}/{username}")
	public ResponseEntity<Response<String>> delete(@RequestHeader("Authorization") String authorization,
			@PathVariable String productTypeId, @PathVariable String username) {
		Response<String> response = new Response<>();
		logger.info("[UserPreferenceProductTypeController][delete][productTypeId: {}][username: {}]", productTypeId,
				username);
		Optional<UserPreferenceProductType> userPreferenceProductType = userPreferenceProductTypeService
				.findByUsernameAndProductTypeId(productTypeId, username);

		if (userPreferenceProductType.isPresent()) {
			userPreferenceProductTypeService.delete(userPreferenceProductType.get().getId());
			response.setData("UserPreferenceProductType removed.");
			logger.info("[UserPreferenceProductTypeController][delete][response: {}]", response.toString());
		} else {
			response.getErrors().add("Register not found");
			logger.error("[UserPreferenceProductTypeController][delete][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}

		return ResponseEntity.ok(response);
	}

	private void validateCreatePartner(UserPreferenceProductType userPreferenceProductType, BindingResult result) {
		if (userPreferenceProductType.getUsername() == null) {
			result.addError(new ObjectError("UserPreferenceProductType", "usernameNotInformed"));
		}
		if (userPreferenceProductType.getProductType() == null) {
			result.addError(new ObjectError("UserPreferenceProductType", "productTypeNotInformed"));
		}
	}

	private void validateUpdatePartner(UserPreferenceProductType userPreferenceProductType, BindingResult result) {
		if (userPreferenceProductType.getUsername() == null) {
			result.addError(new ObjectError("UserPreferenceProductType", "usernameNotInformed"));
		}
		if (userPreferenceProductType.getProductType() == null) {
			result.addError(new ObjectError("UserPreferenceProductType", "productTypeNotInformed"));
		}
	}
}
