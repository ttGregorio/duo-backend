package br.com.nex2you.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.nex2you.api.entity.UserPreferenceProductType;

public interface UserPreferenceProductTypeRepository extends MongoRepository<UserPreferenceProductType, String> {

	List<UserPreferenceProductType> findByUsername(String username);
	
	Optional<UserPreferenceProductType> findByUsernameAndProductTypeId(String username, String productTypeId);

}
