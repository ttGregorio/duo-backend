package br.com.nex2you.api.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.nex2you.api.entity.UserPreferenceProductType;
import br.com.nex2you.api.repository.UserPreferenceProductTypeRepository;
import br.com.nex2you.api.service.UserPreferenceProductTypeService;

@Service
public class UserPreferenceProductTypeServiceImpl implements UserPreferenceProductTypeService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UserPreferenceProductTypeRepository userPreferenceProductTypeRepository;

	@Override
	public UserPreferenceProductType createOrUpdate(UserPreferenceProductType userPreferenceProductType) {
		logger.info("[UserPreferenceProductTypeService][createOrUpdate][userPreferenceProductType: {}]",
				userPreferenceProductType.toString());
		return userPreferenceProductTypeRepository.save(userPreferenceProductType);
	}

	@Override
	public Optional<UserPreferenceProductType> findById(String authorization, String id) {
		logger.info("[UserPreferenceProductTypeService][findById][id: {}]", id);
		return userPreferenceProductTypeRepository.findById(id);
	}

	@Override
	public void delete(String id) {
		logger.info("[UserPreferenceProductTypeService][delete][id: {}]", id);
		userPreferenceProductTypeRepository.deleteById(id);
	}

	@Override
	public List<UserPreferenceProductType> findAll(String username) {
		logger.info("[UserPreferenceProductTypeService][findAll][username: {}]", username);
		return userPreferenceProductTypeRepository.findByUsername(username);
	}

	@Override
	public Optional<UserPreferenceProductType> findByUsernameAndProductTypeId(String productTypeId, String username) {
		logger.info(
				"[UserPreferenceProductTypeService][findByUsernameAndProductTypeId][productTypeId: {}][username: {}]",
				productTypeId, username);
		return userPreferenceProductTypeRepository.findByUsernameAndProductTypeId(username, productTypeId);
	}
}