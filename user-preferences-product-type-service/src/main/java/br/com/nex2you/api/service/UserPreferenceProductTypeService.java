package br.com.nex2you.api.service;

import java.util.List;
import java.util.Optional;

import br.com.nex2you.api.entity.UserPreferenceProductType;

public interface UserPreferenceProductTypeService {

	UserPreferenceProductType createOrUpdate(UserPreferenceProductType userPreferenceProductType);

	Optional<UserPreferenceProductType> findById(String authorization, String id);

	void delete(String id);

	List<UserPreferenceProductType> findAll(String username);

	Optional<UserPreferenceProductType> findByUsernameAndProductTypeId(String productTypeId, String username);

	List<UserPreferenceProductType> findByUsername(String username);
}
