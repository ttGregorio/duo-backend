package br.com.nex2you.api.feign;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import br.com.nex2you.api.entity.User;
import br.com.nex2you.api.response.Response;

@FeignClient(name = "user-service")
@RibbonClient(name = "user-service")
public interface UserClient {

	@GetMapping(value = "/username/{username}")
	public ResponseEntity<Response<User>> findByUsername(@RequestHeader("Authorization") String authorization,
			@PathVariable String username);
}
