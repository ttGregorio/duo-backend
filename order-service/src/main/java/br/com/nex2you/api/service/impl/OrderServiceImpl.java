package br.com.nex2you.api.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import br.com.nex2you.api.entity.Order;
import br.com.nex2you.api.entity.ProductImage;
import br.com.nex2you.api.entity.enums.PaymentStatusEnum;
import br.com.nex2you.api.feign.ProductImageClient;
import br.com.nex2you.api.feign.UserClient;
import br.com.nex2you.api.repository.OrderRepository;
import br.com.nex2you.api.response.Response;
import br.com.nex2you.api.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private UserClient userClient;

	@Autowired
	private ProductImageClient productImageClient;

	@Override
	public Order createOrUpdate(String authorization, Order order) {
		logger.info("[OrderService][createOrUpdate][order: {}]", order.toString());
		return orderRepository.save(order);
	}

	@Override
	public Optional<Order> findById(String id) {
		logger.info("[OrderService][findById][id: {}]", id);
		return orderRepository.findById(id);
	}

	@Override
	public void delete(String authorization, String id) {
		logger.info("[OrderService][delete][id: {}]", id);

		Optional<Order> optional = orderRepository.findById(id);
		if (optional.isPresent()) {
			Order order = optional.get();
			order.setStatus(PaymentStatusEnum.CANCELED);
			orderRepository.save(order);
		}
	}

	@Override
	public Page<Order> findAll(String authorization, int page, int count, String partnerId) {
		logger.info("[OrderService][findAll][page: {}][count: {}][partnerId: {}]", page, count, partnerId);
		PageRequest pageRequest = PageRequest.of(page, count);

		Page<Order> pg = orderRepository.findByPartnerId(pageRequest, partnerId);

		pg.getContent().stream().forEach(order -> {
			order.setUser(userClient.findByUsername(authorization, order.getUsername()).getBody().getData());
		});
		return pg;
	}

	@Override
	public List<Order> findAll(String partnerId) {
		logger.info("[OrderService][findAll][partnerId: {}]", partnerId);
		return orderRepository.findAll();
	}

	@Override
	public Order findSumAll(String partnerId) {
		logger.info("[OrderService][findSumAll][partnerId: {}]", partnerId);
		Order order = new Order();
		List<Order> orders = orderRepository.findAll();
		order.setTotalValue(new BigDecimal(0));
		orders.stream().forEach(item -> {
			order.setTotalValue(order.getTotalValue().add(item.getTotalValue()));
		});

		return order;
	}

	@Override
	public List<Order> findByUsername(String authorization, int page, int count, String username) {
		logger.info("[OrderService][findByUsername][page: {}][count: {}][username: {}]", page, count, username);

		PageRequest pageRequest = PageRequest.of(page, count);

		Page<Order> pg = orderRepository.findByUsername(pageRequest, username);
/*
		pg.stream().forEach(order -> {
			Response<String> teste = productImageClient.findByFirstPictureByProductId(authorization, order.getProduct().getId());

			ProductImage productImage = new ProductImage();
			productImage.setName(teste.getData());

			List<ProductImage> productImages = new ArrayList<ProductImage>();
			productImages.add(productImage);
			order.getProduct().setProductImages(productImages);
		});
		*/
		return pg.getContent();
	}
}