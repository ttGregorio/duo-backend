package br.com.nex2you.api.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import br.com.nex2you.api.entity.Order;

public interface OrderService {

	Order createOrUpdate(String authorization, Order order);

	Optional<Order> findById(String id);

	void delete(String authorization, String id);

	Page<Order> findAll(String authorization, int page, int count, String partnerId);

	List<Order> findAll(String partnerId);

	Order findSumAll(String partnerId);

	List<Order> findByUsername(String authorization, int page, int count, String username);
}
