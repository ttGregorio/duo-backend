package br.com.nex2you.api.entity.enums;

public enum PaymentStatusEnum {
	OPEN, ACCEPTED, REJECTED, CANCELED
}
