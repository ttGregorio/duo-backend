package br.com.nex2you.api.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import br.com.nex2you.api.entity.Order;
import br.com.nex2you.api.response.Response;
import br.com.nex2you.api.service.OrderService;

@RestController
public class OrderController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private OrderService orderService;

	@PostMapping
	public ResponseEntity<Response<Order>> create(@RequestHeader("Authorization") String authorization,
			HttpServletRequest request, @RequestBody Order order, BindingResult result) {
		logger.info("[OrderController][create][request: {}][order: {}]", request.toString(), order.toString());
		Response<Order> response = new Response<>();
		try {
			validateCreatePartner(order, result);
			if (result.hasErrors()) {
				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
				return ResponseEntity.badRequest().body(response);
			}

			if (order.getDate() == null) {
				order.setDate(new Date());
			}

			Order orderPersisted = (Order) orderService.createOrUpdate(authorization, order);
			response.setData(orderPersisted);
			logger.info("[OrderController][create][response: {}]", response.toString());
		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			logger.error("[OrderController][create][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(response);
	}

	@PutMapping
	public ResponseEntity<Response<Order>> update(@RequestHeader("Authorization") String authorization,
			HttpServletRequest request, @RequestBody Order order, BindingResult result) {
		Response<Order> response = new Response<>();
		logger.info("[OrderController][update][request: {}][order: {}]", request.toString(), order.toString());
		try {
			validateUpdatePartner(order, result);
			if (result.hasErrors()) {
				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
				return ResponseEntity.badRequest().body(response);
			}

			Order partnerPersisted = (Order) orderService.createOrUpdate(authorization, order);
			response.setData(partnerPersisted);
			logger.info("[OrderController][update][response: {}]", response.toString());
		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			logger.error("[OrderController][update][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "{id}/id")
	public ResponseEntity<Response<Order>> findById(@PathVariable String id) {
		Response<Order> response = new Response<>();
		logger.info("[OrderController][findById][id: {}]", id);

		Optional<Order> order = orderService.findById(id);

		if (order.isPresent()) {
			response.setData(order.get());
			logger.info("[OrderController][update][response: {}]", response.toString());
		} else {
			response.getErrors().add("Register not found for id ".concat(id));
			logger.error("[OrderController][update][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}

		return ResponseEntity.ok(response);
	}

	@DeleteMapping(value = "{id}")
	public ResponseEntity<Response<String>> delete(@RequestHeader("Authorization") String authorization,
			@PathVariable String id) {
		Response<String> response = new Response<>();
		logger.info("[OrderController][delete][id: {}]", id);
		Optional<Order> order = orderService.findById(id);

		if (order.isPresent()) {
			orderService.delete(authorization, id);
			response.setData("Order ".concat(id).concat(" removed."));
			logger.info("[OrderController][delete][response: {}]", response.toString());
		} else {
			response.getErrors().add("Register not found for id ".concat(id));
			logger.error("[OrderController][delete][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}

		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "{page}/{count}/{partnerId}")
	public ResponseEntity<Response<Page<Order>>> findAll(@RequestHeader("Authorization") String authorization,
			@PathVariable int page, @PathVariable int count, @PathVariable String partnerId) {
		Response<Page<Order>> response = new Response<>();
		logger.info("[OrderController][findAll][page: {}][count: {}][partnerId: {}]", page, count, partnerId);

		Page<Order> orders = orderService.findAll(authorization, page, count, partnerId);

		response.setData(orders);
		logger.info("[OrderController][findAll][response: {}]", response.toString());

		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "{page}/{count}/{username}/user")
	public ResponseEntity<Response<List<Order>>> findByUsername(@RequestHeader("Authorization") String authorization,
			@PathVariable int page, @PathVariable int count, @PathVariable String username) {
		Response<List<Order>> response = new Response<>();
		logger.info("[OrderController][findByUsername][page: {}][count: {}][username: {}]", page, count, username);

		List<Order> orders = orderService.findByUsername(authorization, page, count, username);

		response.setData(orders);
		logger.info("[OrderController][findByUsername][response: {}]", response.toString());

		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "/{orderId}")
	public ResponseEntity<Response<List<Order>>> findAll(@PathVariable String orderId) {
		Response<List<Order>> response = new Response<>();
		logger.info("[OrderController][findAll][orderId: {}]", orderId);

		List<Order> orders = orderService.findAll(orderId);

		response.setData(orders);
		logger.info("[OrderController][findAll][response: {}]", response.toString());

		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "/{orderId}/sum")
	public ResponseEntity<Response<Order>> findSumAll(@PathVariable String orderId) {
		Response<Order> response = new Response<>();
		logger.info("[OrderController][findSumAll][orderId: {}]", orderId);

		Order orders = orderService.findSumAll(orderId);

		response.setData(orders);
		logger.info("[OrderController][findSumAll][response: {}]", response.toString());

		return ResponseEntity.ok(response);
	}

	private void validateCreatePartner(Order order, BindingResult result) {

	}

	private void validateUpdatePartner(Order order, BindingResult result) {

		if (order.getId() == null) {
			result.addError(new ObjectError("Order", "idNotInformed"));
		}
	}
}
