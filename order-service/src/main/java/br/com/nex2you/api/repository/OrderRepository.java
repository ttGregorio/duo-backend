package br.com.nex2you.api.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.nex2you.api.entity.Order;

public interface OrderRepository extends MongoRepository<Order, String> {

	List<Order> findByPartnerIdEquals(String partnerId);

	Page<Order> findByPartnerId(Pageable pageRequest, String partnerId);

	Page<Order> findByUsername(Pageable pageRequest, String username);
}
