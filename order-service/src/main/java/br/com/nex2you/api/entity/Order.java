package br.com.nex2you.api.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import br.com.nex2you.api.entity.enums.PaymentStatusEnum;

@Document
public class Order implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	private Date date;

	private String username;

	@Transient
	private User user;

	@DBRef
	private Partner partner;

	@DBRef
	private ProductType productType;

	@DBRef
	private ProductSubType productSubType;

	@DBRef
	private Product product;

	private String observation;

	private BigDecimal quantity;

	private BigDecimal unityValue;

	private BigDecimal totalValue;

	private PaymentStatusEnum status;

	private String paymentResponse;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the partner
	 */
	public Partner getPartner() {
		return partner;
	}

	/**
	 * @param partner the partner to set
	 */
	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	/**
	 * @return the productType
	 */
	public ProductType getProductType() {
		return productType;
	}

	/**
	 * @param productType the productType to set
	 */
	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	/**
	 * @return the productSubType
	 */
	public ProductSubType getProductSubType() {
		return productSubType;
	}

	/**
	 * @param productSubType the productSubType to set
	 */
	public void setProductSubType(ProductSubType productSubType) {
		this.productSubType = productSubType;
	}

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}

	/**
	 * @param product the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}

	/**
	 * @return the observation
	 */
	public String getObservation() {
		return observation;
	}

	/**
	 * @param observation the observation to set
	 */
	public void setObservation(String observation) {
		this.observation = observation;
	}

	/**
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the unityValue
	 */
	public BigDecimal getUnityValue() {
		return unityValue;
	}

	/**
	 * @param unityValue the unityValue to set
	 */
	public void setUnityValue(BigDecimal unityValue) {
		this.unityValue = unityValue;
	}

	/**
	 * @return the totalValue
	 */
	public BigDecimal getTotalValue() {
		return totalValue;
	}

	/**
	 * @param totalValue the totalValue to set
	 */
	public void setTotalValue(BigDecimal totalValue) {
		this.totalValue = totalValue;
	}

	/**
	 * @return the status
	 */
	public PaymentStatusEnum getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(PaymentStatusEnum status) {
		this.status = status;
	}

	/**
	 * @return the paymentResponse
	 */
	public String getPaymentResponse() {
		return paymentResponse;
	}

	/**
	 * @param paymentResponse the paymentResponse to set
	 */
	public void setPaymentResponse(String paymentResponse) {
		this.paymentResponse = paymentResponse;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", date=" + date + ", username=" + username + ", user=" + user + ", partner="
				+ partner + ", productType=" + productType + ", productSubType=" + productSubType + ", product="
				+ product + ", observation=" + observation + ", quantity=" + quantity + ", unityValue=" + unityValue
				+ ", totalValue=" + totalValue + ", status=" + status + ", paymentResponse=" + paymentResponse + "]";
	}

}