package br.com.nex2you.api.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import br.com.nex2you.api.entity.UserPreferenceProductSubType;
import br.com.nex2you.api.response.Response;
import br.com.nex2you.api.service.UserPreferenceProductSubTypeService;

@RestController
public class UserPreferenceProductSubTypeController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UserPreferenceProductSubTypeService userPreferenceProductSubTypeService;

	@PostMapping
	public ResponseEntity<Response<UserPreferenceProductSubType>> create(HttpServletRequest request,
			@RequestBody UserPreferenceProductSubType userPreferenceProductSubType, BindingResult result) {
		logger.info("[UserPreferenceProductSubTypeController][create][request: {}][userPreferenceProductType: {}]",
				request.toString(), userPreferenceProductSubType.toString());
		Response<UserPreferenceProductSubType> response = new Response<>();
		try {
			validateCreatePartner(userPreferenceProductSubType, result);
			if (result.hasErrors()) {
				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
				return ResponseEntity.badRequest().body(response);
			}

			Optional<UserPreferenceProductSubType> optional = userPreferenceProductSubTypeService
					.findByUsernameAndProductSubTypeId(userPreferenceProductSubType.getProductSubType().getId(),
							userPreferenceProductSubType.getUsername());
			if (!optional.isPresent()) {
				UserPreferenceProductSubType partnerPersisted = (UserPreferenceProductSubType) userPreferenceProductSubTypeService
						.createOrUpdate(userPreferenceProductSubType);
				response.setData(partnerPersisted);
			} else {
				userPreferenceProductSubTypeService.delete(optional.get().getId());
//				response.setData(optional.get());
			}
			logger.info("[UserPreferenceProductSubTypeController][create][response: {}]", response.toString());
		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			logger.error("[UserPreferenceProductSubTypeController][create][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(response);
	}

	@PutMapping
	public ResponseEntity<Response<UserPreferenceProductSubType>> update(HttpServletRequest request,
			@RequestBody UserPreferenceProductSubType userPreferenceProductSubType, BindingResult result) {
		Response<UserPreferenceProductSubType> response = new Response<>();
		logger.info("[UserPreferenceProductSubTypeController][update][request: {}][userPreferenceProductType: {}]",
				request.toString(), userPreferenceProductSubType.toString());
		try {
			validateUpdatePartner(userPreferenceProductSubType, result);
			if (result.hasErrors()) {
				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
				return ResponseEntity.badRequest().body(response);
			}

			UserPreferenceProductSubType partnerPersisted = (UserPreferenceProductSubType) userPreferenceProductSubTypeService
					.createOrUpdate(userPreferenceProductSubType);
			response.setData(partnerPersisted);
			logger.info("[UserPreferenceProductSubTypeController][update][response: {}]", response.toString());
		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			logger.error("[UserPreferenceProductSubTypeController][update][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "{id}/id")
	public ResponseEntity<Response<UserPreferenceProductSubType>> findById(
			@RequestHeader("Authorization") String authorization, @PathVariable String id) {
		Response<UserPreferenceProductSubType> response = new Response<>();
		logger.info("[UserPreferenceProductSubTypeController][findById][id: {}]", id);

		Optional<UserPreferenceProductSubType> userPreferenceProductSubType = userPreferenceProductSubTypeService
				.findById(authorization, id);

		if (userPreferenceProductSubType.isPresent()) {
			response.setData(userPreferenceProductSubType.get());
			logger.info("[UserPreferenceProductSubTypeController][findById][response: {}]", response.toString());
		} else {
			response.getErrors().add("Register not found for id ".concat(id));
			logger.error("[UserPreferenceProductSubTypeController][findById][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}

		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "{productTypeId}/{username}")
	public ResponseEntity<Response<UserPreferenceProductSubType>> findByUsernameAndProductTypeId(
			@PathVariable String productTypeId, @PathVariable String username) {
		Response<UserPreferenceProductSubType> response = new Response<>();
		logger.info(
				"[UserPreferenceProductSubTypeController][findByUsernameAndProductTypeId][productTypeId: {}][username: {}]",
				productTypeId, username);

		Optional<UserPreferenceProductSubType> userPreferenceProductSubType = userPreferenceProductSubTypeService
				.findByUsernameAndProductSubTypeId(productTypeId, username);

		if (userPreferenceProductSubType.isPresent()) {
			response.setData(userPreferenceProductSubType.get());
			logger.info("[UserPreferenceProductSubTypeController][findByUsernameAndProductTypeId][response: {}]",
					response.toString());
		} else {
			response.getErrors().add("Register not found");
			logger.error("[UserPreferenceProductSubTypeController][findByUsernameAndProductTypeId][response: {}]",
					response.toString());
		}

		return ResponseEntity.ok(response);
	}

	@DeleteMapping(value = "{productTypeId}/{username}")
	public ResponseEntity<Response<String>> delete(@RequestHeader("Authorization") String authorization,
			@PathVariable String productTypeId, @PathVariable String username) {
		Response<String> response = new Response<>();
		logger.info("[UserPreferenceProductSubTypeController][delete][productTypeId: {}][username: {}]", productTypeId,
				username);
		Optional<UserPreferenceProductSubType> userPreferenceProductSubType = userPreferenceProductSubTypeService
				.findByUsernameAndProductSubTypeId(productTypeId, username);

		if (userPreferenceProductSubType.isPresent()) {
			userPreferenceProductSubTypeService.delete(userPreferenceProductSubType.get().getId());
			response.setData("UserPreferenceProductSubType removed.");
			logger.info("[UserPreferenceProductSubTypeController][delete][response: {}]", response.toString());
		} else {
			response.getErrors().add("Register not found");
			logger.error("[UserPreferenceProductSubTypeController][delete][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}

		return ResponseEntity.ok(response);
	}

	private void validateCreatePartner(UserPreferenceProductSubType userPreferenceProductSubType,
			BindingResult result) {
		if (userPreferenceProductSubType.getUsername() == null) {
			result.addError(new ObjectError("UserPreferenceProductSubType", "usernameNotInformed"));
		}
		if (userPreferenceProductSubType.getProductSubType() == null) {
			result.addError(new ObjectError("UserPreferenceProductSubType", "productTypeNotInformed"));
		}
	}

	private void validateUpdatePartner(UserPreferenceProductSubType userPreferenceProductSubType,
			BindingResult result) {
		if (userPreferenceProductSubType.getUsername() == null) {
			result.addError(new ObjectError("UserPreferenceProductSubType", "usernameNotInformed"));
		}
		if (userPreferenceProductSubType.getProductSubType() == null) {
			result.addError(new ObjectError("UserPreferenceProductSubType", "productTypeNotInformed"));
		}
	}
}
