package br.com.nex2you.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.nex2you.api.entity.UserPreferenceProductSubType;

public interface UserPreferenceProductSubTypeRepository extends MongoRepository<UserPreferenceProductSubType, String> {

	List<UserPreferenceProductSubType> findByUsername(String username);
	
	Optional<UserPreferenceProductSubType> findByUsernameAndProductSubTypeId(String username, String productTypeId);

}
