package br.com.nex2you.api.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.nex2you.api.entity.UserPreferenceProductSubType;
import br.com.nex2you.api.repository.UserPreferenceProductSubTypeRepository;
import br.com.nex2you.api.service.UserPreferenceProductSubTypeService;

@Service
public class UserPreferenceProductSubTypeServiceImpl implements UserPreferenceProductSubTypeService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UserPreferenceProductSubTypeRepository userPreferenceProductSubTypeRepository;

	@Override
	public UserPreferenceProductSubType createOrUpdate(UserPreferenceProductSubType userPreferenceProductSubType) {
		logger.info("[UserPreferenceProductSubTypeService][createOrUpdate][userPreferenceProductType: {}]",
				userPreferenceProductSubType.toString());
		return userPreferenceProductSubTypeRepository.save(userPreferenceProductSubType);
	}

	@Override
	public Optional<UserPreferenceProductSubType> findById(String authorization, String id) {
		logger.info("[UserPreferenceProductSubTypeService][findById][id: {}]", id);
		return userPreferenceProductSubTypeRepository.findById(id);
	}

	@Override
	public void delete(String id) {
		logger.info("[UserPreferenceProductSubTypeService][delete][id: {}]", id);
		userPreferenceProductSubTypeRepository.deleteById(id);
	}

	@Override
	public List<UserPreferenceProductSubType> findAll(String username) {
		logger.info("[UserPreferenceProductSubTypeService][findAll][username: {}]", username);
		return userPreferenceProductSubTypeRepository.findByUsername(username);
	}

	@Override
	public Optional<UserPreferenceProductSubType> findByUsernameAndProductSubTypeId(String productTypeId, String username) {
		logger.info(
				"[UserPreferenceProductSubTypeService][findByUsernameAndProductTypeId][productTypeId: {}][username: {}]",
				productTypeId, username);
		return userPreferenceProductSubTypeRepository.findByUsernameAndProductSubTypeId(username, productTypeId);
	}
}