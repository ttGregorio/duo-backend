package br.com.nex2you.api.service;

import java.util.List;
import java.util.Optional;

import br.com.nex2you.api.entity.UserPreferenceProductSubType;

public interface UserPreferenceProductSubTypeService {

	UserPreferenceProductSubType createOrUpdate(UserPreferenceProductSubType userPreferenceProductSubType);

	Optional<UserPreferenceProductSubType> findById(String authorization, String id);

	void delete(String id);

	List<UserPreferenceProductSubType> findAll(String username);

	Optional<UserPreferenceProductSubType> findByUsernameAndProductSubTypeId(String productTypeId, String username);
}
