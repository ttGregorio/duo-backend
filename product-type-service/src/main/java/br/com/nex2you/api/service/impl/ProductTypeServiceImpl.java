package br.com.nex2you.api.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import br.com.nex2you.api.entity.ProductType;
import br.com.nex2you.api.entity.UserPreferenceProductType;
import br.com.nex2you.api.feign.UserPreferenceClient;
import br.com.nex2you.api.repository.ProductTypeRepository;
import br.com.nex2you.api.response.Response;
import br.com.nex2you.api.service.ProductTypeService;

@Service
public class ProductTypeServiceImpl implements ProductTypeService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ProductTypeRepository productTypeRepository;

	@Autowired
	private UserPreferenceClient userPreferenceClient;

	@Override
	public ProductType createOrUpdate(ProductType productType) {
		logger.info("[ProductTypeService][createOrUpdate][productType: {}]", productType.toString());
		return productTypeRepository.save(productType);
	}

	@Override
	public Optional<ProductType> findById(String id) {
		logger.info("[ProductTypeService][findById][id: {}]", id);
		return productTypeRepository.findById(id);
	}

	@Override
	public void delete(String id) {
		logger.info("[ProductTypeService][delete][id: {}]", id);
		productTypeRepository.deleteById(id);
	}

	@Override
	public Page<ProductType> findAll(int page, int count) {
		logger.info("[ProductTypeService][findAll][page: {}][count: {}]", page, count);
		PageRequest pageRequest = PageRequest.of(page, count);
		return productTypeRepository.findAll(pageRequest);
	}

	@Override
	public List<ProductType> findAll() {
		logger.info("[ProductTypeService][findAll]");
		return productTypeRepository.findAll();
	}

	@Override
	public List<ProductType> findByUsername(String authorization, String username) {
		logger.info("[ProductTypeService][findByUsername][username: {}]", username);
		List<ProductType> productTypes = productTypeRepository.findAll();

		for (ProductType productType : productTypes) {
			Response<UserPreferenceProductType> response = userPreferenceClient
					.findByUsernameAndProductTypeId(authorization, productType.getId(), username).getBody();
			if(response.getData() != null) {
				productType.setColor("medium");
			}else {
				productType.setColor("secondary");
			}
		}

		return productTypes;
	}
}