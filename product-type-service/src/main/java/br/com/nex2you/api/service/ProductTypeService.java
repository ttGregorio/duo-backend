package br.com.nex2you.api.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import br.com.nex2you.api.entity.ProductType;

public interface ProductTypeService {

	ProductType createOrUpdate(ProductType productType);

	Optional<ProductType> findById(String id);

	void delete(String id);

	Page<ProductType> findAll(int page, int count);

	List<ProductType> findAll();

	List<ProductType> findByUsername(String authorization, String username);
}
