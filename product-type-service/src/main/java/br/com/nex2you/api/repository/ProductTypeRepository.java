package br.com.nex2you.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.nex2you.api.entity.ProductType;

public interface ProductTypeRepository extends MongoRepository<ProductType, String> {
}
