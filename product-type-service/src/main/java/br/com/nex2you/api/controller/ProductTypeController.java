package br.com.nex2you.api.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import br.com.nex2you.api.entity.ProductType;
import br.com.nex2you.api.response.Response;
import br.com.nex2you.api.service.ProductTypeService;

@RestController
public class ProductTypeController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ProductTypeService productTypeService;

	@PostMapping
	public ResponseEntity<Response<ProductType>> create(HttpServletRequest request,
			@RequestBody ProductType productType, BindingResult result) {
		logger.info("[ProductTypeController][create][request: {}][partner: {}]", request.toString(),
				productType.toString());
		Response<ProductType> response = new Response<>();
		try {
			validateCreatePartner(productType, result);
			if (result.hasErrors()) {
				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
				return ResponseEntity.badRequest().body(response);
			}

			ProductType partnerPersisted = (ProductType) productTypeService.createOrUpdate(productType);
			response.setData(partnerPersisted);
			logger.info("[ProductTypeController][create][response: {}]", response.toString());
		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			logger.error("[ProductTypeController][create][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(response);
	}

	@PutMapping
	public ResponseEntity<Response<ProductType>> update(HttpServletRequest request,
			@RequestBody ProductType productType, BindingResult result) {
		Response<ProductType> response = new Response<>();
		logger.info("[ProductTypeController][update][request: {}][partner: {}]", request.toString(),
				productType.toString());
		try {
			validateUpdatePartner(productType, result);
			if (result.hasErrors()) {
				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
				return ResponseEntity.badRequest().body(response);
			}

			ProductType partnerPersisted = (ProductType) productTypeService.createOrUpdate(productType);
			response.setData(partnerPersisted);
			logger.info("[ProductTypeController][update][response: {}]", response.toString());
		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			logger.error("[ProductTypeController][update][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "{id}")
	public ResponseEntity<Response<ProductType>> findById(@PathVariable String id) {
		Response<ProductType> response = new Response<>();
		logger.info("[ProductTypeController][findById][id: {}]", id);

		Optional<ProductType> productType = productTypeService.findById(id);

		if (productType.isPresent()) {
			response.setData(productType.get());
			logger.info("[ProductTypeController][update][response: {}]", response.toString());
		} else {
			response.getErrors().add("Register not found for id ".concat(id));
			logger.error("[ProductTypeController][update][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}

		return ResponseEntity.ok(response);
	}

	@DeleteMapping(value = "{id}")
	public ResponseEntity<Response<String>> delete(@PathVariable String id) {
		Response<String> response = new Response<>();
		logger.info("[ProductTypeController][delete][id: {}]", id);
		Optional<ProductType> productType = productTypeService.findById(id);

		if (productType.isPresent()) {
			productTypeService.delete(id);
			response.setData("ProductType ".concat(id).concat(" removed."));
			logger.info("[ProductTypeController][delete][response: {}]", response.toString());
		} else {
			response.getErrors().add("Register not found for id ".concat(id));
			logger.error("[ProductTypeController][delete][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}

		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "{page}/{count}")
	public ResponseEntity<Response<Page<ProductType>>> findAll(@PathVariable int page, @PathVariable int count) {
		Response<Page<ProductType>> response = new Response<>();
		logger.info("[ProductTypeController][findAll][page: {}][count: {}]", page, count);

		Page<ProductType> productTypes = productTypeService.findAll(page, count);

		response.setData(productTypes);
		logger.info("[ProductTypeController][findAll][response: {}]", response.toString());

		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "{username}/username")
	public ResponseEntity<Response<List<ProductType>>> findByUsername(
			@RequestHeader("Authorization") String authorization, @PathVariable String username) {
		Response<List<ProductType>> response = new Response<>();
		logger.info("[ProductTypeController][findByUsername][username: {}]", username);

		List<ProductType> productTypes = productTypeService.findByUsername(authorization,username);

		response.setData(productTypes);
		logger.info("[ProductTypeController][findAll][response: {}]", response.toString());

		return ResponseEntity.ok(response);
	}

	@GetMapping
	public ResponseEntity<Response<List<ProductType>>> findAll() {
		Response<List<ProductType>> response = new Response<>();
		logger.info("[ProductTypeController][findAll]");

		List<ProductType> productTypes = productTypeService.findAll();

		response.setData(productTypes);
		logger.info("[ProductTypeController][findAll][response: {}]", response.toString());

		return ResponseEntity.ok(response);
	}

	private void validateCreatePartner(ProductType productType, BindingResult result) {
		if (productType.getName() == null) {
			result.addError(new ObjectError("ProductType", "nameNotInformed"));
		}
	}

	private void validateUpdatePartner(ProductType productType, BindingResult result) {
		if (productType.getName() == null) {
			result.addError(new ObjectError("ProductType", "nameNotInformed"));
		}
		if (productType.getId() == null) {
			result.addError(new ObjectError("ProductType", "idNotInformed"));
		}
	}
}
