package br.com.nex2you.api.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.nex2you.api.entity.Partner;
import br.com.nex2you.api.response.Response;
import br.com.nex2you.api.service.PartnerService;

@RestController
public class PartnerController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private PartnerService partnerService;

	@PostMapping
	public ResponseEntity<Response<Partner>> create(HttpServletRequest request, @RequestBody Partner partner,
			BindingResult result) {
		logger.info("[PartnerController][create][request: {}][partner: {}]", request.toString(), partner.toString());
		Response<Partner> response = new Response<>();
		try {
			validateCreatePartner(partner, result);
			if (result.hasErrors()) {
				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
				return ResponseEntity.badRequest().body(response);
			}

			Partner partnerPersisted = (Partner) partnerService.createOrUpdate(partner);
			response.setData(partnerPersisted);
			logger.info("[PartnerController][create][response: {}]", response.toString());
		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			logger.error("[PartnerController][create][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(response);
	}

	@PutMapping
	public ResponseEntity<Response<Partner>> update(HttpServletRequest request, @RequestBody Partner partner,
			BindingResult result) {
		Response<Partner> response = new Response<>();
		logger.info("[PartnerController][update][request: {}][partner: {}]", request.toString(), partner.toString());
		try {
			validateUpdatePartner(partner, result);
			if (result.hasErrors()) {
				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
				return ResponseEntity.badRequest().body(response);
			}

			Partner partnerPersisted = (Partner) partnerService.createOrUpdate(partner);
			response.setData(partnerPersisted);
			logger.info("[PartnerController][update][response: {}]", response.toString());
		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			logger.error("[PartnerController][update][response: {}]", response.toString());
		return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "{id}")
	public ResponseEntity<Response<Partner>> findById(@PathVariable String id) {
		Response<Partner> response = new Response<>();
		logger.info("[PartnerController][findById][id: {}]", id);

		Optional<Partner> partner = partnerService.findById(id);

		if (partner.isPresent()) {
			response.setData(partner.get());
			logger.info("[PartnerController][update][response: {}]", response.toString());
		} else {
			response.getErrors().add("Register not found for id ".concat(id));
			logger.error("[PartnerController][update][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}

		return ResponseEntity.ok(response);
	}

	@DeleteMapping(value = "{id}")
	public ResponseEntity<Response<String>> delete(@PathVariable String id) {
		Response<String> response = new Response<>();
		logger.info("[PartnerController][delete][id: {}]", id);
		Optional<Partner> partner = partnerService.findById(id);

		if (partner.isPresent()) {
			partnerService.delete(id);
			response.setData("Partner ".concat(id).concat(" removed."));
			logger.info("[PartnerController][delete][response: {}]", response.toString());
		} else {
			response.getErrors().add("Register not found for id ".concat(id));
			logger.error("[PartnerController][delete][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}

		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "{page}/{count}")
	public ResponseEntity<Response<Page<Partner>>> findAll(@PathVariable int page, @PathVariable int count) {
		Response<Page<Partner>> response = new Response<>();
		logger.info("[PartnerController][findAll][page: {}][count: {}]", page, count);

		Page<Partner> partners = partnerService.findAll(page, count);

		response.setData(partners);
		logger.info("[PartnerController][findAll][response: {}]", response.toString());

		return ResponseEntity.ok(response);

	}

	@GetMapping
	public ResponseEntity<Response<List<Partner>>> findAll() {
		Response<List<Partner>> response = new Response<>();
		logger.info("[PartnerController][findAll]");

		List<Partner> partners = partnerService.findAll();

		response.setData(partners);
		logger.info("[PartnerController][findAll][response: {}]", response.toString());

		return ResponseEntity.ok(response);
	}
	
	@GetMapping(value = "{partnerId}/image")
	public ResponseEntity<Response<String>> getImageWithMediaType(@PathVariable String partnerId) throws IOException {
		Response<String> response = new Response<>();

		response.setData(partnerService.findImageById(partnerId));

		return ResponseEntity.ok(response);
	}

	private void validateCreatePartner(Partner partner, BindingResult result) {
		if (partner.getName() == null) {
			result.addError(new ObjectError("Partner", "nameNotInformed"));
		}
	}

	private void validateUpdatePartner(Partner partner, BindingResult result) {
		if (partner.getName() == null) {
			result.addError(new ObjectError("Partner", "nameNotInformed"));
		}
		if (partner.getId() == null) {
			result.addError(new ObjectError("Partner", "idNotInformed"));
		}
	}
}
