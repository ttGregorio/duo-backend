package br.com.nex2you.api.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import br.com.nex2you.api.entity.Partner;

public interface PartnerService {

	Partner createOrUpdate(Partner partner);

	Optional<Partner> findById(String id);

	void delete(String id);

	Page<Partner> findAll(int page, int count);

	List<Partner> findAll();

	String findImageById(String partnerId) throws IOException;
}
