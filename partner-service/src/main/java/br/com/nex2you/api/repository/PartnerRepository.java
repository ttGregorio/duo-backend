package br.com.nex2you.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.nex2you.api.entity.Partner;

public interface PartnerRepository extends MongoRepository<Partner, String> {
}
