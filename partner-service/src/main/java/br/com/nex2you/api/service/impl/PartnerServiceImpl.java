package br.com.nex2you.api.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import br.com.nex2you.api.entity.Partner;
import br.com.nex2you.api.repository.PartnerRepository;
import br.com.nex2you.api.service.PartnerService;

@Service
public class PartnerServiceImpl implements PartnerService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private PartnerRepository partnerRepository;

	@Value("${images-base-directory}")
	private String imageBaseDirectory;

	@Override
	public Partner createOrUpdate(Partner partner) {
		logger.info("[PartnerService][createOrUpdate][partner: {}]", partner.toString());
		
		String image = partner.getLogo();
		
		partner.setLogo(null);
		
		partner = partnerRepository.save(partner);

		File f = new File(imageBaseDirectory.concat("\\").concat(partner.getId()));
		if (!f.exists()) {
			f.mkdir();
		}
		String[] imgTmp = image.split(",");
		byte[] decodedImg = Base64.getDecoder().decode(imgTmp[1].getBytes(StandardCharsets.UTF_8));
		Path destinationFile = Paths.get(imageBaseDirectory.concat("\\").concat(partner.getId()), partner.getId());
		partner.setLogo(partner.getId());

		try {
			Files.write(destinationFile, decodedImg);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return partnerRepository.save(partner);
	}

	@Override
	public Optional<Partner> findById(String id) {
		logger.info("[PartnerService][findById][id: {}]", id);
		return partnerRepository.findById(id);
	}

	@Override
	public void delete(String id) {
		logger.info("[PartnerService][delete][id: {}]", id);
		partnerRepository.deleteById(id);
	}

	@Override
	public Page<Partner> findAll(int page, int count) {
		logger.info("[PartnerService][findAll][page: {}][count: {}]", page, count);
		PageRequest pageRequest = PageRequest.of(page, count);
		return partnerRepository.findAll(pageRequest);
	}

	@Override
	public List<Partner> findAll() {
		logger.info("[PartnerService][findAll]");
		return partnerRepository.findAll();
	}

	@Override
	public String findImageById(String partnerId) throws IOException {
		logger.info("[PartnerService][findImageById][partnerId: {}]", partnerId);

		Optional<Partner> optional = partnerRepository.findById(partnerId);

		if (optional.isPresent()) {
			File file = new File(imageBaseDirectory.concat("\\").concat(partnerId).concat("\\").concat(partnerId));

			byte[] encoded = Base64.getEncoder().encode(FileUtils.readFileToByteArray(file));

			return "data:image/png;base64,".concat(new String(encoded, StandardCharsets.UTF_8));
		}

		return "https://sdumont.lncc.br/images/projects/no-image.png";
	}
}