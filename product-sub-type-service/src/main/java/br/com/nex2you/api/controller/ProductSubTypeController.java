package br.com.nex2you.api.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import br.com.nex2you.api.entity.ProductSubType;
import br.com.nex2you.api.entity.ProductType;
import br.com.nex2you.api.response.Response;
import br.com.nex2you.api.service.ProductSubTypeService;

@RestController
public class ProductSubTypeController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ProductSubTypeService productSubTypeService;

	@PostMapping
	public ResponseEntity<Response<ProductSubType>> create(HttpServletRequest request,
			@RequestBody ProductSubType productType, BindingResult result) {
		logger.info("[ProductSubTypeController][create][request: {}][partner: {}]", request.toString(),
				productType.toString());
		Response<ProductSubType> response = new Response<>();
		try {
			validateCreatePartner(productType, result);
			if (result.hasErrors()) {
				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
				return ResponseEntity.badRequest().body(response);
			}

			ProductSubType partnerPersisted = (ProductSubType) productSubTypeService.createOrUpdate(productType);
			response.setData(partnerPersisted);
			logger.info("[ProductSubTypeController][create][response: {}]", response.toString());
		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			logger.error("[ProductSubTypeController][create][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(response);
	}

	@PutMapping
	public ResponseEntity<Response<ProductSubType>> update(HttpServletRequest request,
			@RequestBody ProductSubType productType, BindingResult result) {
		Response<ProductSubType> response = new Response<>();
		logger.info("[ProductSubTypeController][update][request: {}][partner: {}]", request.toString(),
				productType.toString());
		try {
			validateUpdatePartner(productType, result);
			if (result.hasErrors()) {
				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
				return ResponseEntity.badRequest().body(response);
			}

			ProductSubType partnerPersisted = (ProductSubType) productSubTypeService.createOrUpdate(productType);
			response.setData(partnerPersisted);
			logger.info("[ProductSubTypeController][update][response: {}]", response.toString());
		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			logger.error("[ProductSubTypeController][update][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "{id}/id")
	public ResponseEntity<Response<ProductSubType>> findById(@PathVariable String id) {
		Response<ProductSubType> response = new Response<>();
		logger.info("[ProductSubTypeController][findById][id: {}]", id);

		Optional<ProductSubType> productType = productSubTypeService.findById(id);

		if (productType.isPresent()) {
			response.setData(productType.get());
			logger.info("[ProductSubTypeController][update][response: {}]", response.toString());
		} else {
			response.getErrors().add("Register not found for id ".concat(id));
			logger.error("[ProductSubTypeController][update][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}

		return ResponseEntity.ok(response);
	}
	
	@GetMapping(value = "{productTypeId}/{username}/username")
	public ResponseEntity<Response<List<ProductSubType>>> findByUsername(
			@RequestHeader("Authorization") String authorization, @PathVariable String productTypeId, @PathVariable String username) {
		Response<List<ProductSubType>> response = new Response<>();
		logger.info("[ProductTypeController][findByUsername][username: {}]", username);

		List<ProductSubType> productTypes = productSubTypeService.findByUsername(authorization,productTypeId, username);

		response.setData(productTypes);
		logger.info("[ProductTypeController][findAll][response: {}]", response.toString());

		return ResponseEntity.ok(response);
	}

	@DeleteMapping(value = "{id}")
	public ResponseEntity<Response<String>> delete(@PathVariable String id) {
		Response<String> response = new Response<>();
		logger.info("[ProductSubTypeController][delete][id: {}]", id);
		Optional<ProductSubType> productType = productSubTypeService.findById(id);

		if (productType.isPresent()) {
			productSubTypeService.delete(id);
			response.setData("ProductSubType ".concat(id).concat(" removed."));
			logger.info("[ProductSubTypeController][delete][response: {}]", response.toString());
		} else {
			response.getErrors().add("Register not found for id ".concat(id));
			logger.error("[ProductSubTypeController][delete][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}

		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "{page}/{count}/{productTypeId}")
	public ResponseEntity<Response<Page<ProductSubType>>> findAll(@PathVariable int page, @PathVariable int count,
			@PathVariable String productTypeId) {
		Response<Page<ProductSubType>> response = new Response<>();
		logger.info("[ProductSubTypeController][findAll][page: {}][count: {}][productTypeId: {}]", page, count,
				productTypeId);

		Page<ProductSubType> productTypes = productSubTypeService.findAll(page, count, productTypeId);

		response.setData(productTypes);
		logger.info("[ProductSubTypeController][findAll][response: {}]", response.toString());

		return ResponseEntity.ok(response);

	}

	@GetMapping(value = "/{productTypeId}")
	public ResponseEntity<Response<List<ProductSubType>>> findAll(@PathVariable String productTypeId) {
		Response<List<ProductSubType>> response = new Response<>();
		logger.info("[ProductSubTypeController][findAll]");

		List<ProductSubType> productTypes = productSubTypeService.findAll(productTypeId);

		response.setData(productTypes);
		logger.info("[ProductSubTypeController][findAll][response: {}]", response.toString());

		return ResponseEntity.ok(response);

	}

	private void validateCreatePartner(ProductSubType productType, BindingResult result) {
		if (productType.getName() == null) {
			result.addError(new ObjectError("ProductSubType", "nameNotInformed"));
		}
	}

	private void validateUpdatePartner(ProductSubType productType, BindingResult result) {
		if (productType.getName() == null) {
			result.addError(new ObjectError("ProductSubType", "nameNotInformed"));
		}
		if (productType.getId() == null) {
			result.addError(new ObjectError("ProductSubType", "idNotInformed"));
		}
	}
}
