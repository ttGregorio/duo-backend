package br.com.nex2you.api.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import br.com.nex2you.api.entity.ProductSubType;
import br.com.nex2you.api.entity.UserPreferenceProductSubType;
import br.com.nex2you.api.feign.UserPreferenceSubTypeClient;
import br.com.nex2you.api.repository.ProductSubTypeRepository;
import br.com.nex2you.api.response.Response;
import br.com.nex2you.api.service.ProductSubTypeService;

@Service
public class ProductSubTypeServiceImpl implements ProductSubTypeService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ProductSubTypeRepository productSubTypeRepository;

	@Autowired
	private UserPreferenceSubTypeClient userPreferenceSubTypeClient;

	@Override
	public ProductSubType createOrUpdate(ProductSubType productSubType) {
		logger.info("[ProductSubTypeService][createOrUpdate][productSubType: {}]", productSubType.toString());
		return productSubTypeRepository.save(productSubType);
	}

	@Override
	public Optional<ProductSubType> findById(String id) {
		logger.info("[ProductSubTypeService][findById][id: {}]", id);
		return productSubTypeRepository.findById(id);
	}

	@Override
	public void delete(String id) {
		logger.info("[ProductSubTypeService][delete][id: {}]", id);
		productSubTypeRepository.deleteById(id);
	}

	@Override
	public Page<ProductSubType> findAll(int page, int count, String productTypeId) {
		logger.info("[ProductSubTypeService][findAll][page: {}][count: {}][productTypeId: {}]", page, count,
				productTypeId);
		PageRequest pageRequest = PageRequest.of(page, count);
		return productSubTypeRepository.findByProductTypeId(pageRequest, productTypeId);
	}

	@Override
	public List<ProductSubType> findAll(String productTypeId) {
		logger.info("[ProductSubTypeService][findAll][productTypeId: {}]", productTypeId);
		return productSubTypeRepository.findByProductTypeIdEquals(productTypeId);
	}

	@Override
	public List<ProductSubType> findByUsername(String authorization, String productTypeId, String username) {
		logger.info("[ProductTypeService][findByUsername][productTypeId: {}][username: {}]", productTypeId,
				username);
		List<ProductSubType> productTypes = productSubTypeRepository.findByProductTypeIdEquals(productTypeId);

		for (ProductSubType productSubType : productTypes) {
			Response<UserPreferenceProductSubType> response = userPreferenceSubTypeClient
					.findByUsernameAndProductTypeId(authorization, productSubType.getId(), username).getBody();
			if (response.getData() != null) {
				productSubType.setColor("medium");
			} else {
				productSubType.setColor("secondary");
			}
		}

		return productTypes;
	}
}