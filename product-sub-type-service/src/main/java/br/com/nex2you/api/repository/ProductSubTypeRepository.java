package br.com.nex2you.api.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.nex2you.api.entity.ProductSubType;
import br.com.nex2you.api.entity.ProductType;

public interface ProductSubTypeRepository extends MongoRepository<ProductSubType, String> {

	List<ProductSubType> findByProductTypeIdEquals(String productTypeId);

	Page<ProductSubType> findByProductTypeIdEquals(Pageable pageRequest, String productTypeId);

	Page<ProductSubType> findByProductTypeIdLike(Pageable pageRequest, String productTypeId);

	Page<ProductSubType> findByProductTypeEquals(Pageable pageRequest, ProductType productType);

	Page<ProductSubType> findByProductTypeId(Pageable pageRequest, String productTypeId);
}
