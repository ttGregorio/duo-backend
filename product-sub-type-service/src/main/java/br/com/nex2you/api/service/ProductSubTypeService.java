package br.com.nex2you.api.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import br.com.nex2you.api.entity.ProductSubType;
import br.com.nex2you.api.entity.ProductType;

public interface ProductSubTypeService {

	ProductSubType createOrUpdate(ProductSubType ProductSubType);

	Optional<ProductSubType> findById(String id);

	void delete(String id);

	Page<ProductSubType> findAll(int page, int count, String productTypeId);

	List<ProductSubType> findAll(String productTypeId);

	List<ProductSubType> findByUsername(String authorization, String productTypeId, String username);
}
