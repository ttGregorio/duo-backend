package br.com.nex2you.api.feign;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import br.com.nex2you.api.entity.UserPreferenceProductSubType;
import br.com.nex2you.api.response.Response;

@FeignClient(name = "user-preferences-product-sub-type-service")
@RibbonClient(name = "user-preferences-product-sub-type-service")
public interface UserPreferenceSubTypeClient {

	@GetMapping(value = "{productSubTypeId}/{username}")
	public ResponseEntity<Response<UserPreferenceProductSubType>> findByUsernameAndProductTypeId(
			@RequestHeader("Authorization") String authorization, @PathVariable String productSubTypeId,
			@PathVariable String username);
}
