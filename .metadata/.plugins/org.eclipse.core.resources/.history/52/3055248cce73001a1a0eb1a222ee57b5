package br.com.nex2you.api.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.nex2you.api.entity.ProductType;
import br.com.nex2you.api.response.Response;
import br.com.nex2you.api.service.ProductTypeService;

@RestController
@RequestMapping("/api/product-type")
@CrossOrigin(origins = "*")
public class ProductTypeController {

	@Autowired
	private ProductTypeService productTypeService;

	@PostMapping
	@PreAuthorize("hasAnyRole('ADMIN')")
	public ResponseEntity<Response<ProductType>> create(HttpServletRequest request, @RequestBody ProductType productType,
			BindingResult result) {
		Response<ProductType> response = new Response<>();
		try {
			validateCreateProductType(productType, result);
			if (result.hasErrors()) {
				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
				return ResponseEntity.badRequest().body(response);
			}

			ProductType productTypePersisted = (ProductType) productTypeService.createOrUpdate(productType);
			response.setData(productTypePersisted);
		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(response);
	}

	@PutMapping
	@PreAuthorize("hasAnyRole('ADMIN')")
	public ResponseEntity<Response<ProductType>> update(HttpServletRequest request, @RequestBody ProductType productType,
			BindingResult result) {
		Response<ProductType> response = new Response<>();
		try {
			validateUpdateProductType(productType, result);
			if (result.hasErrors()) {
				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
				return ResponseEntity.badRequest().body(response);
			}

			ProductType productTypePersisted = (ProductType) productTypeService.createOrUpdate(productType);
			response.setData(productTypePersisted);
		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "{id}")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public ResponseEntity<Response<ProductType>> findById(@PathVariable String id) {
		Response<ProductType> response = new Response<>();
		Optional<ProductType> productType = productTypeService.findById(id);

		if (productType.isPresent()) {
			response.setData(productType.get());
		} else {
			response.getErrors().add("Register not found for id ".concat(id));
			return ResponseEntity.badRequest().body(response);
		}

		return ResponseEntity.ok(response);
	}

	@DeleteMapping(value = "{id}")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public ResponseEntity<Response<String>> delete(@PathVariable String id) {
		Response<String> response = new Response<>();
		Optional<ProductType> productType = productTypeService.findById(id);

		if (productType.isPresent()) {
			productTypeService.delete(id);
			response.setData("ProductType ".concat(id).concat(" removed."));
		} else {
			response.getErrors().add("Register not found for id ".concat(id));
			return ResponseEntity.badRequest().body(response);
		}

		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "{page}/{count}")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public ResponseEntity<Response<Page<ProductType>>> findAll(@PathVariable int page, @PathVariable int count) {
		Response<Page<ProductType>> response = new Response<>();

		Page<ProductType> productTypes = productTypeService.findAll(page, count);

		response.setData(productTypes);

		return ResponseEntity.ok(response);

	}

	private void validateCreateProductType(ProductType productType, BindingResult result) {
		if (productType.getName() == null) {
			result.addError(new ObjectError("ProductType", "nameNotInformed"));
		}
	}

	private void validateUpdateProductType(ProductType productType, BindingResult result) {
		if (productType.getName() == null) {
			result.addError(new ObjectError("ProductType", "nameNotInformed"));
		}
		if (productType.getId() == null) {
			result.addError(new ObjectError("ProductType", "idNotInformed"));
		}
	}
}
