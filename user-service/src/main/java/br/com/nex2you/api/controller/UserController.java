package br.com.nex2you.api.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.nex2you.api.model.User;
import br.com.nex2you.api.response.Response;
import br.com.nex2you.api.service.UserService;

@RestController
//@CrossOrigin(origins = "*")
public class UserController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UserService userService;

	@PostMapping
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ResponseEntity<?> createUser(@RequestBody User userDTO) {
		try {
			logger.info("[UserController][createUser][userDTO: ".concat(userDTO.toString()).concat("]"));
			return ResponseEntity.ok(new Response(userService.createUserInKeyCloak(userDTO)));
		}

		catch (Exception ex) {
			logger.error("[UserController][createUser][".concat(ex.getMessage()).concat("]"));
			ex.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping(value = "/username/{name}")
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ResponseEntity<?> username(@PathVariable String name) {
		logger.info("[UserController][username][name: ".concat(name).concat("]"));
		return ResponseEntity.ok(new Response(userService.findByUsername(name)));
	}

	@GetMapping(value = "/{id}")
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ResponseEntity<?> findById(@PathVariable String id) {
		logger.info("[UserController][findById][namde: ".concat(id).concat("]"));
		return ResponseEntity.ok(new Response(userService.findById(id)));
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping
	public ResponseEntity<?> findAll() {
		logger.info("[UserController][findAll]");
		List<User> list = userService.findAll();
		return ResponseEntity.ok(new Response(list));
	}
}