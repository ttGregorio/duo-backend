package br.com.nex2you.api.service;

import java.util.List;

import br.com.nex2you.api.model.User;

public interface UserService {

	String getToken(User userCredentials);

	String getByRefreshToken(String refreshToken);

	User createUserInKeyCloak(User userDTO);

	void logoutUser(String userId);

	void resetPassword(String newPassword, String userId);
	
	User findByUsername(String username);

	User findById(String id);

	List<User> findAll();
}