package br.com.nex2you.api.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.nex2you.api.model.User;
import br.com.nex2you.api.service.UserService;

@Component
public class UserServiceImpl implements UserService {

	@Value("${keycloak.credentials.secret}")
	private String SECRETKEY;

	@Value("${keycloak.resource}")
	private String CLIENTID;

	@Value("${keycloak.auth-server-url}")
	private String AUTHURL;

	@Value("${keycloak.realm}")
	private String REALM;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.eleevait.api.service.impl.UserService#getToken(br.com.eleevait.api.
	 * model.UserCredentials)
	 */
	@Override
	public String getToken(User userCredentials) {

		String responseToken = null;
		try {

			String username = userCredentials.getUsername();

			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			urlParameters.add(new BasicNameValuePair("grant_type", "password"));
			urlParameters.add(new BasicNameValuePair("client_id", CLIENTID));
			urlParameters.add(new BasicNameValuePair("username", username));
			urlParameters.add(new BasicNameValuePair("password", userCredentials.getPassword()));
			urlParameters.add(new BasicNameValuePair("client_secret", SECRETKEY));

			responseToken = sendPost(urlParameters);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseToken;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.eleevait.api.service.impl.UserService#getByRefreshToken(java.lang.
	 * String)
	 */
	@Override
	public String getByRefreshToken(String refreshToken) {

		String responseToken = null;
		try {

			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			urlParameters.add(new BasicNameValuePair("grant_type", "refresh_token"));
			urlParameters.add(new BasicNameValuePair("client_id", CLIENTID));
			urlParameters.add(new BasicNameValuePair("refresh_token", refreshToken));
			urlParameters.add(new BasicNameValuePair("client_secret", SECRETKEY));

			responseToken = sendPost(urlParameters);

		} catch (Exception e) {
			e.printStackTrace();

		}

		return responseToken;
	}

	@Override
	public User createUserInKeyCloak(User userDTO) {
		logger.debug("[UserService][createUserInKeyCloak][userDTO: {}]", userDTO);

		Map<String, List<String>> attributes = new HashMap<String, List<String>>();
		attributes.put("language", Arrays.asList(userDTO.getLanguage()));
		attributes.put("partnerId", Arrays.asList(userDTO.getPartnerId()));

		UserRepresentation user = new UserRepresentation();
		user.setUsername(userDTO.getEmail());
		user.setEmail(userDTO.getEmail());
		user.setFirstName(userDTO.getFirstName());
		user.setLastName(userDTO.getLastName());
		user.setEnabled(true);
		user.setAttributes(attributes);
		user.setId(userDTO.getId());

		UsersResource userRessource = getRealmResource().users();

		Response response = userRessource.create(user);

		logger.debug("[UserService][createUserInKeyCloak][response: {}]", response.getStatusInfo());

		return findByUsername(userDTO.getUsername());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.eleevait.api.service.impl.UserService#logoutUser(java.lang.String)
	 */
	@Override
	public void logoutUser(String userId) {
		logger.debug("[UserService][logoutUser][userId: {}]", userId);

		UsersResource userRessource = getKeycloakUserResource();

		userRessource.get(userId).logout();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.eleevait.api.service.impl.UserService#resetPassword(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public void resetPassword(String newPassword, String userId) {
		logger.debug("[UserService][resetPassword][newPassword: {}][userId: {}]", newPassword, userId);

		UsersResource userResource = getKeycloakUserResource();

		CredentialRepresentation passwordCred = new CredentialRepresentation();
		passwordCred.setTemporary(false);
		passwordCred.setType(CredentialRepresentation.PASSWORD);
		passwordCred.setValue(newPassword.toString().trim());

		userResource.get(userId).resetPassword(passwordCred);

	}

	private UsersResource getKeycloakUserResource() {
		logger.debug("[UserService][getKeycloakUserResource]");

		Keycloak kc = KeycloakBuilder.builder().serverUrl(AUTHURL).realm("master").username("admin")
				.password("123123123").clientId("admin-cli")
				.resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build()).build();

		RealmResource realmResource = kc.realm(REALM);
		UsersResource userRessource = realmResource.users();

		return userRessource;
	}

	private RealmResource getRealmResource() {
		logger.debug("[UserService][getRealmResource]");

		Keycloak kc = KeycloakBuilder.builder().serverUrl(AUTHURL).realm("master").grantType(OAuth2Constants.PASSWORD)
				.clientId("admin-cli")
//				.clientSecret(clientSecret)
				.username("admin").password("123123123").build();

		RealmResource realmResource = kc.realm(REALM);

		return realmResource;

	}

	private String sendPost(List<NameValuePair> urlParameters) throws Exception {
		logger.debug("[UserService][sendPost][urlParameters: ".concat(urlParameters.toString()).concat("]"));

		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(AUTHURL + "/realms/" + REALM + "/protocol/openid-connect/token");

		post.setEntity(new UrlEncodedFormEntity(urlParameters));

		HttpResponse response = client.execute(post);

		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		logger.debug("[UserService][sendPost][result: {}", result.toString());

		return result.toString();

	}

	@Override
	public User findByUsername(String username) {
		logger.info("[UserService][findByUsername][username: ".concat(username).concat("]"));
		UsersResource userRessource = getKeycloakUserResource();

		List<UserRepresentation> itens = userRessource.search(username);

		for (UserRepresentation userRepresentation : itens) {
			logger.info("[UserService][findByUsername][user: ".concat(userRepresentation.toString()).concat("]"));
			return new User(userRepresentation.getId(), userRepresentation.getFirstName(),
					userRepresentation.getLastName(), userRepresentation.getEmail(), null,
					userRepresentation.getUsername(), userRepresentation.getAttributes().get("language").get(0),
					userRepresentation.getAttributes().get("partnerId") != null
							? userRepresentation.getAttributes().get("partnerId").get(0)
							: null);
		}
		logger.info("[UserService][findByUsername][not found]");

		return null;
	}

	@Override
	public User findById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> findAll() {
		logger.info("[UserService][findAll]");
		List<User> users = new ArrayList<User>();
		UsersResource userRessource = getKeycloakUserResource();

		for (UserRepresentation userRepresentation : userRessource.list()) {
			users.add(new User(userRepresentation.getId(), userRepresentation.getFirstName(),
					userRepresentation.getLastName(), userRepresentation.getEmail(), null,
					userRepresentation.getUsername(), userRepresentation.getAttributes().get("language").get(0),
					userRepresentation.getAttributes().get("partnerId").size() > 0
							? userRepresentation.getAttributes().get("partnerId").get(0)
							: null));
		}
		logger.info("[UserService][findAll][users: {}", users.toString());

		return users;
	}

}