package br.com.nex2you.api.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.nex2you.api.entity.ProductImage;

public interface ProductImageRepository extends MongoRepository<ProductImage, String> {
	List<ProductImage> findByProductId(String productId);
}
