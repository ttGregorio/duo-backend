package br.com.nex2you.api.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.nex2you.api.entity.ProductImage;
import br.com.nex2you.api.response.Response;
import br.com.nex2you.api.service.ProductImageService;

@RestController
public class ProductImageController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ProductImageService productImageService;

	@PostMapping
	public ResponseEntity<Response<ProductImage>> create(HttpServletRequest request, @RequestBody ProductImage product,
			BindingResult result) {
		logger.info("[ProductImageController][create][request: {}][product: {}]", request.toString(),
				product.toString());
		Response<ProductImage> response = new Response<>();
		try {
			validateCreatePartner(product, result);
			if (result.hasErrors()) {
				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
				return ResponseEntity.badRequest().body(response);
			}

			ProductImage partnerPersisted = (ProductImage) productImageService.createOrUpdate(product);
			response.setData(partnerPersisted);
			logger.info("[ProductImageController][create][response: {}]", response.toString());
		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			logger.error("[ProductImageController][create][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(response);
	}

	@PutMapping
	public ResponseEntity<Response<ProductImage>> update(HttpServletRequest request, @RequestBody ProductImage product,
			BindingResult result) {
		Response<ProductImage> response = new Response<>();
		logger.info("[ProductImageController][update][request: {}][partner: {}]", request.toString(),
				product.toString());
		try {
			validateUpdatePartner(product, result);
			if (result.hasErrors()) {
				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
				return ResponseEntity.badRequest().body(response);
			}

			ProductImage partnerPersisted = (ProductImage) productImageService.createOrUpdate(product);
			response.setData(partnerPersisted);
			logger.info("[ProductImageController][update][response: {}]", response.toString());
		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			logger.error("[ProductImageController][update][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "{id}/id")
	public ResponseEntity<Response<ProductImage>> findById(@PathVariable String id) {
		Response<ProductImage> response = new Response<>();
		logger.info("[ProductImageController][findById][id: {}]", id);

		Optional<ProductImage> product = productImageService.findById(id);

		if (product.isPresent()) {
			response.setData(product.get());
			logger.info("[ProductImageController][update][response: {}]", response.toString());
		} else {
			response.getErrors().add("Register not found for id ".concat(id));
			logger.error("[ProductImageController][update][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}

		return ResponseEntity.ok(response);
	}

	@DeleteMapping(value = "{id}")
	public ResponseEntity<Response<String>> delete(@PathVariable String id) {
		Response<String> response = new Response<>();
		logger.info("[ProductImageController][delete][id: {}]", id);
		Optional<ProductImage> product = productImageService.findById(id);

		if (product.isPresent()) {
			productImageService.delete(id);
			response.setData("ProductImage ".concat(id).concat(" removed."));
			logger.info("[ProductImageController][delete][response: {}]", response.toString());
		} else {
			response.getErrors().add("Register not found for id ".concat(id));
			logger.error("[ProductImageController][delete][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}

		return ResponseEntity.ok(response);
	}

	@DeleteMapping(value = "{productId}/product")
	public ResponseEntity<Response<String>> deleteByProduct(@PathVariable String productId) {
		Response<String> response = new Response<>();
		logger.info("[ProductImageController][deleteByProduct][productId: {}]", productId);
		List<ProductImage> products = productImageService.findAll(productId);

		if (products.size() > 0) {
			products.stream().forEach(productImage -> {
				productImageService.delete(productImage.getId());
			});
			response.setData("ProductImages for product ".concat(productId).concat(" removed."));
			logger.info("[ProductImageController][deleteByProduct][response: {}]", response.toString());
		} else {
			response.getErrors().add("Register not found for id ".concat(productId));
			logger.error("[ProductImageController][deleteByProduct][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}

		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "/{productId}")
	public ResponseEntity<Response<List<ProductImage>>> findAll(@PathVariable String productId) {
		Response<List<ProductImage>> response = new Response<>();
		logger.info("[ProductImageController][findAll][productId: {}]", productId);

		List<ProductImage> products = productImageService.findAll(productId);

		response.setData(products);
		logger.info("[ProductImageController][findAll][response: {}]", response.toString());

		return ResponseEntity.ok(response);

	}

	@GetMapping(value = "{productId}/{name}/image")
	public ResponseEntity<Response<String>> getImageWithMediaType(@PathVariable String productId,
			@PathVariable String name) throws IOException {
		Response<String> response = new Response<>();

		response.setData(productImageService.findImageByName(productId, name));

		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "{productId}/first-image")
	public ResponseEntity<Response<String>> getFirstImage(@PathVariable String productId) throws IOException {
		Response<String> response = new Response<>();

		response.setData(productImageService.findFirstByProductId(productId));

		return ResponseEntity.ok(response);
	}

	
	private void validateCreatePartner(ProductImage product, BindingResult result) {
		if (product.getImage() == null) {
			result.addError(new ObjectError("ProductImage", "imageNotInformed"));
		}
	}

	private void validateUpdatePartner(ProductImage product, BindingResult result) {
		if (product.getImage() == null) {
			result.addError(new ObjectError("ProductImage", "imageNotInformed"));
		}
		if (product.getId() == null) {
			result.addError(new ObjectError("ProductImage", "idNotInformed"));
		}
	}
}
