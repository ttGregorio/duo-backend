package br.com.nex2you.api.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import br.com.nex2you.api.entity.ProductImage;

public interface ProductImageService {

	ProductImage createOrUpdate(ProductImage productImage);

	Optional<ProductImage> findById(String id);

	void delete(String id);

	List<ProductImage> findAll(String productId);

	String findImageByName(String productId, String name) throws IOException;

	String findFirstByProductId(String productId) throws IOException;
}
