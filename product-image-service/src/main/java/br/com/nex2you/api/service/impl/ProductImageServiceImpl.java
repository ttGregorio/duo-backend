package br.com.nex2you.api.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.nex2you.api.entity.ProductImage;
import br.com.nex2you.api.repository.ProductImageRepository;
import br.com.nex2you.api.service.ProductImageService;

@Service
public class ProductImageServiceImpl implements ProductImageService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String ALPHA_NUMERIC_STRING = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	@Autowired
	private ProductImageRepository productImageRepository;

	@Value("${images-base-directory}")
	private String imageBaseDirectory;

	@Override
	public ProductImage createOrUpdate(ProductImage productImage) {
		logger.info("[ProductImageService][createOrUpdate][productImage: {}]", productImage.toString());

		File f = new File(imageBaseDirectory.concat("\\").concat(productImage.getProduct().getId()));
		if (!f.exists()) {
			f.mkdir();
		}
		String[] imgTmp = productImage.getImage().split(",");
		byte[] decodedImg = Base64.getDecoder().decode(imgTmp[1].getBytes(StandardCharsets.UTF_8));
		String filename = generateFileName(20);
		Path destinationFile = Paths.get(imageBaseDirectory.concat("\\").concat(productImage.getProduct().getId()),
				filename);
		productImage.setName(filename);

		try {
			Files.write(destinationFile, decodedImg);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return productImageRepository.save(productImage);
	}

	@Override
	public Optional<ProductImage> findById(String id) {
		logger.info("[ProductImageService][findById][id: {}]", id);
		return productImageRepository.findById(id);
	}

	@Override
	public void delete(String id) {
		logger.info("[ProductImageService][delete][id: {}]", id);
		productImageRepository.deleteById(id);
	}

	@Override
	public List<ProductImage> findAll(String productId) {
		logger.info("[ProductImageService][findAll][productId: {}]", productId);
		return productImageRepository.findByProductId(productId);
	}

	public static String generateFileName(int count) {
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}

	@Override
	public String findImageByName(String productId, String name) throws IOException {
		logger.info("[ProductImageService][findImageByName][productId: {}][name: {}]", productId, name);
		File file = new File(imageBaseDirectory.concat("\\").concat(productId).concat("\\").concat(name));

		byte[] encoded = Base64.getEncoder().encode(FileUtils.readFileToByteArray(file));

		return "data:image/png;base64,".concat(new String(encoded, StandardCharsets.UTF_8));
	}

	@Override
	public String findFirstByProductId(String productId) throws IOException {
		logger.info("[ProductImageService][findFirstByProductId][productId: {}]", productId);
		
		List<ProductImage>productImages = productImageRepository.findByProductId(productId);
				
		if(productImages.size() > 0) {
			return findImageByName(productImages.get(0).getProduct().getId(), productImages.get(0).getName());
		}
				
		return "https://sdumont.lncc.br/images/projects/no-image.png";
	}
}