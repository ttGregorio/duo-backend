package br.com.nex2you.api.feign;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import br.com.nex2you.api.entity.UserPreferenceProductType;
import br.com.nex2you.api.response.Response;

@FeignClient(name = "user-preferences-product-sub-type-service")
@RibbonClient(name = "user-preferences-product-sub-type-service")
public interface UserPreferenceClient {

	@GetMapping(value = "{productTypeId}/{username}")
	public ResponseEntity<Response<UserPreferenceProductType>> findByUsernameAndProductTypeId(
			@RequestHeader("Authorization") String authorization, @PathVariable String productTypeId,
			@PathVariable String username);
}