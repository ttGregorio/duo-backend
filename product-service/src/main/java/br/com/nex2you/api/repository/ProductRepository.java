package br.com.nex2you.api.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.nex2you.api.entity.Product;

public interface ProductRepository extends MongoRepository<Product, String> {

	Page<Product> findByPartnerId(Pageable pageRequest, String partnerId);

	List<Product> findByPartnerIdAndProductTypeIdAndProductSubTypeId(String partnerId, String productType,
			String productSubType);

	List<Product> findByProductTypeId(String productTypeId);

	Page<Product> findByProductTypeIdAndNameContainingIgnoreCase(Pageable pageRequest, String productTypeId, String string);

	List<Product> findByProductTypeIdAndNameContainingIgnoreCaseAndPartnerNameContainingIgnoreCaseAndProductTypeNameContainingIgnoreCase(
			String productTypeId, String productName, String partnerName, String productType);

}
