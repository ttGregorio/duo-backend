package br.com.nex2you.api.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import br.com.nex2you.api.entity.Product;
import br.com.nex2you.api.entity.dto.ProductSearchDTO;

public interface ProductService {

	Product createOrUpdate(Product product);

	Optional<Product> findById(String authorization, String id);

	void delete(String id);

	Page<Product> findAll(int page, int count, String partnerId);

	List<Product> findAll(Product product);

	Optional<Product> findByIdWithoutImages(String id);

	List<Product> findByPartnerIdProductTypeAndProductSubType(String productType, String productSubType,
			String partnerId);

	List<Product> findByUsernameAndProductTypeIdNotIn(int page, int count, String authorization, String username, String productTypeId, ProductSearchDTO productSearchDTO);
}