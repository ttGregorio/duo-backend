package br.com.nex2you.api.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import br.com.nex2you.api.entity.Product;
import br.com.nex2you.api.entity.ProductImage;
import br.com.nex2you.api.entity.dto.ProductSearchDTO;
import br.com.nex2you.api.feign.ProductImageClient;
import br.com.nex2you.api.repository.ProductRepository;
import br.com.nex2you.api.response.Response;
import br.com.nex2you.api.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ProductImageClient productImageClient;

	@Override
	public Product createOrUpdate(Product productSubType) {
		logger.info("[ProductService][createOrUpdate][productSubType: {}]", productSubType.toString());
		return productRepository.save(productSubType);
	}

	@Override
	public Optional<Product> findById(String authorization, String id) {
		logger.info("[ProductService][findById][id: {}]", id);
		Optional<Product> optionalProduct = productRepository.findById(id);
		if (optionalProduct.isPresent()) {
			optionalProduct.get()
					.setProductImages(productImageClient.findByPartnerÏd(authorization, id).getBody().getData());
		}
		return optionalProduct;
	}

	@Override
	public void delete(String id) {
		logger.info("[ProductService][delete][id: {}]", id);
		productRepository.deleteById(id);
	}

	@Override
	public Page<Product> findAll(int page, int count, String partnerId) {
		logger.info("[ProductService][findAll][page: {}][count: {}][partnerId: {}]", page, count, partnerId);
		PageRequest pageRequest = PageRequest.of(page, count);
		return productRepository.findByPartnerId(pageRequest, partnerId);
	}

	@Override
	public List<Product> findAll(Product product) {
		logger.info("[ProductService][findAll][product: {}]", product.toString());
		return productRepository.findAll();
	}

	@Override
	public Optional<Product> findByIdWithoutImages(String id) {
		logger.info("[ProductService][findByIdWithoutImages][id: {}]", id);
		return productRepository.findById(id);
	}

	@Override
	public List<Product> findByPartnerIdProductTypeAndProductSubType(String productType, String productSubType,
			String partnerId) {
		logger.info(
				"[ProductService][findByPartnerIdProductTypeAndProductSubType][productType: {}][productSubType: {}][partnerId: {}]",
				productType, productSubType, partnerId);
		return productRepository.findByPartnerIdAndProductTypeIdAndProductSubTypeId(partnerId, productType,
				productSubType);
	}

	@Override
	public List<Product> findByUsernameAndProductTypeIdNotIn(int page, int count, String authorization, String username,
			String productTypeId, ProductSearchDTO productSearchDTO) {
		logger.info(
				"[ProductService][findByUsernameAndProductTypeIdNotIn][username: {}][productTypeId: {}][productSearchDTO: {}]",
				username, productTypeId, productSearchDTO.toString());
		
		PageRequest pageRequest = PageRequest.of(page, count);
		
		Page<Product> products = productRepository.findByProductTypeIdAndNameContainingIgnoreCase(pageRequest, productTypeId,
				productSearchDTO.getProductName());

		products.stream().forEach(product -> {
			Response<String> teste = productImageClient.findByFirstPictureByProductId(authorization, product.getId());

			ProductImage productImage = new ProductImage();
			productImage.setName(teste.getData());

			List<ProductImage> productImages = new ArrayList<ProductImage>();
			productImages.add(productImage);
			product.setProductImages(productImages);
		});

		return products.getContent();
	}
}