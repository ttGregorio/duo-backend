package br.com.nex2you.api.feign;

import java.util.List;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import br.com.nex2you.api.entity.ProductImage;
import br.com.nex2you.api.response.Response;

@FeignClient(name = "product-image-service")
@RibbonClient(name = "product-image-service")
public interface ProductImageClient {

	@GetMapping(value = "/{partnerId}")
	public ResponseEntity<Response<List<ProductImage>>> findByPartnerÏd(@RequestHeader("Authorization") String authorization,
			@PathVariable String partnerId);
	
	@GetMapping(value = "/{productId}/first-image")
	public Response<String> findByFirstPictureByProductId(@RequestHeader("Authorization") String authorization,
			@PathVariable String productId);
}
