package br.com.nex2you.api.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import br.com.nex2you.api.entity.Product;
import br.com.nex2you.api.entity.dto.ProductSearchDTO;
import br.com.nex2you.api.response.Response;
import br.com.nex2you.api.service.ProductService;

@RestController
public class ProductController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ProductService productService;

	@PostMapping
	public ResponseEntity<Response<Product>> create(HttpServletRequest request, @RequestBody Product product,
			BindingResult result) {
		logger.info("[ProductController][create][request: {}][product: {}]", request.toString(), product.toString());
		Response<Product> response = new Response<>();
		try {
			validateCreatePartner(product, result);
			if (result.hasErrors()) {
				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
				return ResponseEntity.badRequest().body(response);
			}

			Product partnerPersisted = (Product) productService.createOrUpdate(product);
			response.setData(partnerPersisted);
			logger.info("[ProductController][create][response: {}]", response.toString());
		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			logger.error("[ProductController][create][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(response);
	}

	@PutMapping
	public ResponseEntity<Response<Product>> update(HttpServletRequest request, @RequestBody Product product,
			BindingResult result) {
		Response<Product> response = new Response<>();
		logger.info("[ProductController][update][request: {}][partner: {}]", request.toString(), product.toString());
		try {
			validateUpdatePartner(product, result);
			if (result.hasErrors()) {
				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
				return ResponseEntity.badRequest().body(response);
			}

			Product partnerPersisted = (Product) productService.createOrUpdate(product);
			response.setData(partnerPersisted);
			logger.info("[ProductController][update][response: {}]", response.toString());
		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			logger.error("[ProductController][update][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "{id}/id")
	public ResponseEntity<Response<Product>> findById(@RequestHeader("Authorization") String authorization,
			@PathVariable String id) {
		Response<Product> response = new Response<>();
		logger.info("[ProductController][findById][id: {}]", id);

		Optional<Product> product = productService.findById(authorization, id);

		if (product.isPresent()) {
			response.setData(product.get());
			logger.info("[ProductController][update][response: {}]", response.toString());
		} else {
			response.getErrors().add("Register not found for id ".concat(id));
			logger.error("[ProductController][update][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}

		return ResponseEntity.ok(response);
	}

	@DeleteMapping(value = "{id}")
	public ResponseEntity<Response<String>> delete(@RequestHeader("Authorization") String authorization,
			@PathVariable String id) {
		Response<String> response = new Response<>();
		logger.info("[ProductController][delete][id: {}]", id);
		Optional<Product> product = productService.findByIdWithoutImages(id);

		if (product.isPresent()) {
			productService.delete(id);
			response.setData("Product ".concat(id).concat(" removed."));
			logger.info("[ProductController][delete][response: {}]", response.toString());
		} else {
			response.getErrors().add("Register not found for id ".concat(id));
			logger.error("[ProductController][delete][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}

		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "{productType}/{productSubType}/{partnerId}/partner")
	public ResponseEntity<Response<List<Product>>> findByPartnerIdProductTypeAndProductSubType(
			@PathVariable String productType, @PathVariable String productSubType, @PathVariable String partnerId) {
		Response<List<Product>> response = new Response<>();
		logger.info(
				"[ProductController][findByPartnerIdProductTypeAndProductSubType][productType: {}][productSubType: {}][partnerId: {}]",
				productType, productSubType, partnerId);

		List<Product> products = productService.findByPartnerIdProductTypeAndProductSubType(productType, productSubType,
				partnerId);

		response.setData(products);
		logger.info("[ProductController][findByPartnerIdProductTypeAndProductSubType][response: {}]",
				response.toString());

		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "{page}/{count}/{partnerId}")
	public ResponseEntity<Response<Page<Product>>> findAll(@PathVariable int page, @PathVariable int count,
			@PathVariable String partnerId) {
		Response<Page<Product>> response = new Response<>();
		logger.info("[ProductController][findAll][page: {}][count: {}][partnerId: {}]", page, count, partnerId);

		Page<Product> products = productService.findAll(page, count, partnerId);

		response.setData(products);
		logger.info("[ProductController][findAll][response: {}]", response.toString());

		return ResponseEntity.ok(response);
	}

	@PostMapping("{page}/{count}/{username}/{productTypeId}/search")
	public ResponseEntity<Response<List<Product>>> findProducts(@PathVariable int page, @PathVariable int count,
			@RequestHeader("Authorization") String authorization, @PathVariable String username,
			@PathVariable String productTypeId, @RequestBody ProductSearchDTO productSearchDTO) {
		logger.info("[ProductController][findProducts][username: {}][productTypeId: {}][productSearchDTO: {}]",
				username, productTypeId, productSearchDTO.toString());
		Response<List<Product>> response = new Response<>();
		try {
			response.setData(productService.findByUsernameAndProductTypeIdNotIn(page, count, authorization, username,
					productTypeId, productSearchDTO));
			logger.info("[ProductController][findProducts][response: {}]", response.toString());
		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			logger.error("[ProductController][findProducts][response: {}]", response.toString());
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(response);
	}

	private void validateCreatePartner(Product product, BindingResult result) {
		if (product.getName() == null) {
			result.addError(new ObjectError("Product", "nameNotInformed"));
		}
	}

	private void validateUpdatePartner(Product product, BindingResult result) {
		if (product.getName() == null) {
			result.addError(new ObjectError("Product", "nameNotInformed"));
		}
		if (product.getId() == null) {
			result.addError(new ObjectError("Product", "idNotInformed"));
		}
	}
}
